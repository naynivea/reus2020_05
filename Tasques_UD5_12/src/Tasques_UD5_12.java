import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD5_12 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Guardo la contrase�a correcta en una variable tipo String
		String contraCorrecta = "abcf";
		
		//Declaro la variable que guardar� la contrase�a introducida 
		//y la defino como nula para poder compararla
		String contrasenaIntroducida = null;
		
		//Declaro la variable que ser� el contador
		
		int i = 0;
		
		//Hago un bucle while que se repetir� mientras la contrase�a no sea correcta 
		//e intentos sea menor que 3
		while(!contraCorrecta.equals(contrasenaIntroducida) && i < 3) {
			
			//Guardo  la contrase�a introducida
			contrasenaIntroducida = JOptionPane.showInputDialog("Introduce la contrase�a");
		
			//Incremento el contador en 1
			i++;
		}
		
		//Con una estructura condicional if/else if
		// si la contrase�a es correcta mostramos el mensaje Enhorabuena
		// si sobrepasa los intentos mostramos otro mensaje que no ha superado los intentos
		if(contrasenaIntroducida.equals(contraCorrecta)) {
			JOptionPane.showMessageDialog(null, "�Enhorabuena!");
		} else if( i >=3) {
			JOptionPane.showMessageDialog(null, "Lo siento.. Has superado los intentos");
		}
	}

}
