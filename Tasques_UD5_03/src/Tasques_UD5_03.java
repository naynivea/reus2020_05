import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD5_03 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Utilizando la clase JOptionPane dentro del paquete javax.swing 
		//guardamos en una variable
		//tipo String un nombre y le mostramos un mensaje
		
		String nombre = JOptionPane.showInputDialog("Introduce tu nombre");
		
		JOptionPane.showMessageDialog(null, "Bienvenido/a " + nombre);
	}

}
