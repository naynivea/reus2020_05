import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD5_06 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Definimos el valor de la constante IVA 
		final double IVA = 0.21;
		
		//Recibimos el valor del producto por teclado
		// Lo convertimos y guardamos en una variable tipo double
		double valor = Double.parseDouble(JOptionPane.showInputDialog("Introduce el precio"));
		JOptionPane.showMessageDialog(null, "El valor final del producto con el IVA es de: "+ (valor +(valor * IVA)));
	}

}
