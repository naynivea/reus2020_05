import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD5_11 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//En una String guardamos el dia introducido por teclado
		String diaSemana = JOptionPane.showInputDialog("Introduce el d�a de la semana");
		
		//Con la estructura condicional switch 
		// dependiendo del dia introducido mostramos si el d�a es laborable o no
		switch(diaSemana) {
			case "lunes":
				System.out.println("Es un d�a laborable");
				break;
			case "martes":
				System.out.println("Es un d�a laborable");
				break;
			case "mi�rcoles":
				System.out.println("Es un d�a laborable");
				break;
			case "jueves":
				System.out.println("Es un d�a laborable");
				break;
			case "viernes":
				System.out.println("Es un d�a laborable");
				break;
			case "s�bado":
				System.out.println("No es un d�a laborable");
				break;
			case "domingo":
				System.out.println("No es un d�a laborable");
				break;
		}
		

	}

}
