/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD5_08 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Con una estructura de repetici�n for
		//declaramos una variable i con valor inicial de 1 
		//a la que vamos incrementando
		//cada vez que se repita, y que se para c�ando
		//el valor sea menor o igual a 100
		
		for(int i = 1; i <= 100; i++) {
			System.out.println(i);
		}

	}

}
