/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD5_01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		double a = 50.5;
		double b = 50.8;
		
		//Dentro de una estructura condicional veo si los valores son iguales 
		//y si no son, utilizo el m�todo max de la clase Math 
		//y comparo los valores tipo double para saber cu�l es el valor mayor
		
		if(a == b) {
			System.out.println("Son iguales!");
		} else {
			System.out.println(" El mayor es " + Math.max(a, b));
		}
		
	}

}
