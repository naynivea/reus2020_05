/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
import javax.swing.JOptionPane;
public class Tasques_UD5_05 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Recibimos el valor por teclado 
		//en String
		String texto_num = JOptionPane.showInputDialog("Introduce un n�mero y te dir� si la divisi�n por 2 es exacta");
		
		//Convertimos el valor a double y
		//lo guardamos en una variable
		double num = Double.parseDouble(texto_num);
		
		//Utilizando una estructura condicional if/else
		// averiguamos si la divisi�n del n�mero entre dos es exacta
		if(num % 2 == 0) {
			JOptionPane.showMessageDialog(null, "Es divisible entre dos");
		} else {
			JOptionPane.showMessageDialog(null, "No es divisible entre dos");
		}

	}

}
