import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD5_13 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Convierto los operandos introducidos al tipo de la varible 
		//y guardo en las variables los operandos
		double operando1 = Double.parseDouble(JOptionPane.showInputDialog("Introduce el operando 1"));
		double operando2 = Double.parseDouble(JOptionPane.showInputDialog("Introduce el operando 2"));
		
		//Solicito que introduzca que operaci�n quiere ejecutar
		// en una variable String introducida por teclado
		String operacio = JOptionPane.showInputDialog("�Qu� operaci�n quieres hacer? Suma (+) Resta (-) Divisi�n (/) Multiplicaci�n (*) Potenciaci�n (^) M�dulo (%)");
		
		//Utilizo una estructura condicional switch y dependiendo
		//de la operaci�n que introduzca ejecutar� una funci�n
		switch(operacio) {
			case "+":
				//caso suma
				JOptionPane.showMessageDialog(null, "La suma de " + operando1 + " + " + operando2 + " = " + (operando1 + operando2));
				break;
			case "-":	
				//caso resta
				JOptionPane.showMessageDialog(null, "La resta de " + operando1 + " - " + operando2 + " = " + (operando1 - operando2));
				break;
			case "/":
				//caso divisi�n
				JOptionPane.showMessageDialog(null, "La divisi�n de " + operando1 + " / " + operando2 + " = " + (operando1 / operando2));
				break;
			case "*":
				//caso multiplicaci�n
				JOptionPane.showMessageDialog(null, "La multiplicaci�n de " + operando1 + " * " + operando2 + " = " + (operando1 * operando2));
				break;
			case "^":
				//caso potenciaci�n
				JOptionPane.showMessageDialog(null, "La potencia de " + operando1 + " ^ " + operando2 + " = " + (Math.pow(operando1, operando2)));
				break;
			case "%":
				//caso m�dulo
				JOptionPane.showMessageDialog(null, "El m�dulo de " + operando1 + " % " + operando2 + " = " + (operando1 % operando2));
				break;
		}

	}
	
}
