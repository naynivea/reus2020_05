/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD5_07 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Declaramos la variable i y la iniciamos con el valor 1
		int i = 1;
		
		//Hacemos un bucle while que mostra el valor de la variable i
		//mientras sea menor o igual a 100
		while(i <= 100) {
			System.out.println(i);
			
			//incrementamos el valor de i a cada iteración
			i++;
		}

	}

}
