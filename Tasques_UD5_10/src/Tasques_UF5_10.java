import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */

public class Tasques_UF5_10 {
	
	public static void main(String[] args) {
		
		//Guardamos el valor de ventas introducido y convertido a entero 
		// en una variable
		int numVentas = Integer.parseInt(JOptionPane.showInputDialog("Introduce el n�mero de ventas"));
		
		//Declaramos la variable resultado que guardar� el valor
		// final de las ventas realizadas
		double resultado = 0;
		
		//Hacemos un bucle for que se repetir� seg�n el n�mero de ventas
		for(int i = 0; i < numVentas; i++) {
			
			//Guardamos el valor de la venta convertido en double en una variable en cada repetici�n
			double venta = Double.parseDouble(JOptionPane.showInputDialog("Introduce el valor de la venta"));
			
			//A cada iteraci�n sumamos la venta realizada m�s el valor anterior
			//as� se guardar� los valores de todas las ventas
			resultado += venta;
		}
		
		//Mostramos el valor final de todas las ventas
		JOptionPane.showMessageDialog(null, "El valor final de las ventas es de: " + resultado);
	}

}
