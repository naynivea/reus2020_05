/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
public class Tasques_UD5_09 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//En un bucle for iniciamos una variable i con valor 1
		// el bucle se repetir� mientas el valor de i sea menor o igual a 100
		
		for(int i = 1; i <= 100; i++) {
			
			//Con la estructura condicional if 
			//averiguamos si i es divisible por 2 o 3 para
			//mostrarlo por pantalla
			if(i % 2 == 0 || i % 3 == 0) {
				System.out.println(i);
			}
		}
	}

}
