/**
 * 
 */

/**
 * @author Nayara Nivea Gomes Santos
 *
 */
import javax.swing.JOptionPane;
public class Tasques_UD5_04 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Declaramos en una variable el valor de pi
		final double PI = 3.14;
		
		//Recibimos el valor del radio por teclado 
		//utilizando JOptionPane
		String texto_radio = JOptionPane.showInputDialog("Introduce el radio del c�rculo");
		
		//Convertimos el valor a double
		double radio = Double.parseDouble(texto_radio);
		
		//Calculamos el �rea del c�rculo
		double area = PI * (Math.pow(radio, 2));
		
		//Mostramos el valor del �rea del c�rculo
		JOptionPane.showMessageDialog(null, "El �rea del c�rculo es: " + area);

	}

}
